﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControllerFactoryTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class NavigationPage : Page
    {
        public NavigationPage()
        {
            InitializeComponent();
            Title = "Главная страница";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (e.Source is FrameworkElement ctrl)
            {
                if (ctrl.Tag is IViewProvider initr)
                {
                    var page = initr.CreateView();

                    NavigationService.GetNavigationService(this).Navigate(page, initr.DisplayName);
                }
            }
        }
    } 
}
