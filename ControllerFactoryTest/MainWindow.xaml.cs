﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControllerFactoryTest
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool IsNotHome => !(MyFrame.Content is NavigationPage);

        public MainWindow()
        {
            InitializeComponent();
            MyFrame.Content = new NavigationPage();
            MyFrame.Navigated += MyFrame_Navigated;
        }

        private void MyFrame_Navigated(object sender, NavigationEventArgs e)
        {
            if (e.Content is Page page)
            {
                MyFrame.Tag = page.Title;
            }
        }

        private void NavButton_Click(object sender, RoutedEventArgs e)
        {
            if (e.Source is FrameworkElement ctrl)
            {
                ctrl.ContextMenu.IsOpen = true;
            }
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (e.Source is FrameworkElement ctrl && ctrl.Tag is IViewProvider prov)
            {
                var page = prov.CreateView();
                MyFrame.Navigate(page);
            }
        }
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsNotHome)
            {
                var page = new NavigationPage();
                MyFrame.Navigate(page);
            }
        }
    }
}
