﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllerFactoryTest.Models
{
    public sealed class IterationCompletedEventArgs
    {
        public Int32 Iteration { get; }
        public Int32 SwappedFromPosition { get; }
        public Int32 SwappedToPosition { get; }
        public IList<(Int32 pos, Int32 len)> RangedPositions { get; }
        public IList<Int32> TaggedPositions { get; }

        public ISortableCollection ChangedList { get; }

        public IterationCompletedEventArgs(ISortableCollection list, Int32 iteration, 
            Int32 swapFrom, Int32 swapTo, IList<Int32> taggedPositions = null, IList<(Int32, Int32)> rangedPositions = null)
        {
            Iteration = iteration;
            ChangedList = list;

            SwappedFromPosition = swapFrom;
            SwappedToPosition = swapTo;

            TaggedPositions = taggedPositions;
            RangedPositions = rangedPositions;
        }

    }

    public interface ISortableCollection : IList<Int32>, ICollection<Int32>, IEnumerable<Int32>
    {
        event EventHandler<IterationCompletedEventArgs> IterationCompleted;
        void NotifyOnIterationCompleted(IterationCompletedEventArgs e);
        ISortableCollection Clone();
    }

    public static class ICollectionParsingExtensions
    {
        /// <summary>
        /// Получает массив чисел путем парсинга строки
        /// </summary>
        /// <param name="str">строка, содержащая массив</param>
        /// <returns>массив</returns>
        public static T FromContentString<T>(String str)
            where T : IList<Int32>, new()
        {
            var strCollection = str?.Trim().Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            T list = new T();

            foreach (String item in strCollection)
                list.Add(Int32.Parse(item));

            return list;
        }

        /// <summary>
        /// Получает строковое представление массива.
        /// </summary>
        /// <param name="list">массив</param>
        /// <returns>строка</returns>
        public static String ToContentString(this IList<Int32> list)
        {
            Int32 n = list.Count;

            var sb = new StringBuilder();

            foreach(var a in list)
            {
                sb.Append($"{a} ");
            }

            sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }

        /// <summary>
        /// Поулчает строковое представление массива, расставляя метки
        /// </summary>
        /// <param name="swapFrom">
        ///     in: индекс первого переставляемого значения в массиве
        ///     out: индекс первого переставляемого значения в строке
        /// </param>
        /// <param name="swapFromCount">
        ///     длина строкового представления первого переставляемого значения
        /// </param>
        /// <param name="swapTo">
        ///     in: индекс второго переставляемого значения в массиве
        ///     out: индекс второго переставляемого значения в строке
        /// </param>
        /// <param name="swapToCount">
        ///     длина строкового представления второго переставляемого значения
        /// </param>
        /// <param name="tagged">
        ///     массив индексов элементов в массиве, которые должны быть помечены
        /// </param>
        /// <param name="tags">
        ///     массив пар индекс-длина строковых представлений помечаемых элементов
        /// </param>
        /// <param name="ranged">
        ///     массив пар индексов начала и длин диапазонов в массиве
        /// </param>
        /// <param name="ranges">
        ///     in: количество элементов в диапазоне
        ///     out: количество символов в диапазоне
        /// </param>
        /// <returns></returns>
        public static String ToContentString(
            this IList<Int32> list,
            ref Int32 swapFrom,
            out Int32 swapFromCount,
            ref Int32 swapTo,
            out Int32 swapToCount,
            IList<Int32> tagged,
            out IList<(Int32 pos, Int32 len)> tags,
            IList<(Int32 index, Int32 count)> ranged,
            out IList<(Int32 pos, Int32 len)> ranges)
        {
            Int32 n = list.Count;
            Int32 to = swapTo, from = swapFrom;

            tags = new List<(Int32 pos, Int32 len)>(tagged?.Count() ?? 0);
            ranges = new List<(Int32 pos, Int32 len)>(ranged?.Count() ?? 0);
            swapFromCount = swapToCount = 0;

            var sb = new StringBuilder();

            Int32 curPos = 0;
            (Int32 pos, Int32 len) prevRange = (0, 0), substrPtr = (0, 0);

            for (Int32 i = 0; i < list.Count; i++)
            {
                var temp = $"{list[i]} ";

                if (i == from) { swapFrom = curPos; swapFromCount = temp.Length - 1; }
                if (i == to) { swapTo = curPos; swapToCount = temp.Length - 1; }
                if (tagged?.Contains(i) == true) { tags.Add((curPos, temp.Length - 1)); }

                (Int32 pos, Int32 len) curRange = ranged?.FirstOrDefault(
                    item => item.index <= i && item.count >= i - item.index + 1
                ) ?? (0, 0);


                if (!prevRange.Equals(curRange))
                {
                    if (prevRange.len != 0) ranges.Add(substrPtr);
                    substrPtr = (curPos, temp.Length);
                    prevRange = curRange;
                }
                else if (prevRange.len != 0)
                {
                    substrPtr.len += temp.Length;
                }

                curPos += temp.Length;
                sb.Append(temp);
            }

            if (prevRange.len != 0) ranges.Add((substrPtr.pos, substrPtr.len - 1));

            sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }
    }

    public class SortableCollection : Collection<Int32>, ISortableCollection
    {      
        public SortableCollection() : base()
        {

        }

        public event EventHandler<IterationCompletedEventArgs> IterationCompleted = delegate { };
        public void NotifyOnIterationCompleted(IterationCompletedEventArgs e)
        {
            IterationCompleted(this, e);
        }

        public void SelectionSort()
        {
            this.ObservableSelectionSort(false);
        }
        public void ObservableSelectionSort()
        {
            if (this.Count > 50) throw new ArgumentException("Слишком много данных для отображения");
            this.ObservableSelectionSort(true);
        }

        private void ObservableSelectionSort(Boolean notify)
        {
            if (this == null) throw new NullReferenceException();

            Int32 n = this.Count;

            for (Int32 i = 0; i < n - 1; i++)
            {
                for (Int32 j = n - 1; j > i; j--)
                {
                    if (this[i] > this[j])
                    {
                        Int32 t = this[i];
                        this[i] = this[j];
                        this[j] = t;

                        if (notify)
                        {
                            var args = new IterationCompletedEventArgs(this, i + 1, i, j, null, new(Int32, Int32)[] { (0, i + 1) });
                            NotifyOnIterationCompleted(args);
                        }
                    }
                }
            }
        }

        public void BubbleSort()
        {
            this.ObservableBubbleSort(false);
        }
        public void ObservableBubbleSort()
        {
            if (this.Count > 50) throw new ArgumentException("Слишком много данных для отображения");
            this.ObservableBubbleSort(true);
        }

        private void ObservableBubbleSort(Boolean notify)
        {
            Int32 n = this.Count;

            for (Int32 i = 0; i < n - 1; i++)
            {
                for (Int32 j = n - 1; j > i; j--)
                {
                    if (this[j] < this[j - 1])
                    {
                        Int32 t = this[j];
                        this[j] = this[j - 1];
                        this[j - 1] = t;

                        if (notify)
                        {
                            var args = new IterationCompletedEventArgs(this, i + 1, j - 1, j);
                            NotifyOnIterationCompleted(args);
                        }
                    }
                }
            }
        }

        public void ShellSort()
        {
            this.ObservableShellSort(false);
        }
        public void ObservableShellSort()
        {
            if(this.Count > 50) throw new ArgumentException("Слишком много данных для отображения");
            this.ObservableShellSort(true);
        }
        private void ObservableShellSort(Boolean notify)
        {
            Int32 n = this.Count;

            Int32 step = Math.Max(n / 3, 1);
            Int32 it = 0;

            while (step >= 1)
            {
                Int32 numOfSteps = step;

                for (Int32 k = 0; k < numOfSteps; k++)
                {
                    Int32 maxReached = (n / step) * step + k;
                    if (maxReached >= n) maxReached -= step;

                    for (Int32 i = k; i < maxReached; i += step)
                    {
                        it++;

                        for (Int32 j = maxReached; j > i; j -= step)
                        {
                            if (this[i] > this[j])
                            {
                                Int32 t = this[j];
                                this[j] = this[i];
                                this[i] = t;

                                if (notify)
                                {
                                    IterationCompletedEventArgs args = null;
                                    if (step != 1)
                                    {
                                        args = new IterationCompletedEventArgs(this, it, i, j, null,
                                            Enumerable.Range(0, maxReached / step + 1).Select(p => (k + p * step, 1)).ToArray());
                                    }
                                    else
                                    {
                                        args = new IterationCompletedEventArgs(this, it, i, j, null, 
                                            new (Int32, Int32)[] { (0, n) });
                                    }

                                    NotifyOnIterationCompleted(args);
                                }                                
                            }
                        }
                    }
                }

                step /= 2;
            }
        }

        private void ObservableQuickSort(Int32 low, Int32 high, Boolean notify, ref Int32 it)
        {
            it++;

            if(low < high)
            {
                Int32 pos_p = (low + high) / 2;
                Int32 pivot = this[pos_p];

                Int32 i = low, j = high;
                while (i <= j)
                {
                    while (this[i] < pivot) i++;
                    while (this[j] > pivot) j--;

                    
                    if (i < j)
                    {
                        Int32 t = this[i];
                        this[i] = this[j];
                        this[j] = t;

                        if (notify)
                        {
                            var args = new IterationCompletedEventArgs(this, it, i, j, 
                                new Int32[] { pos_p }, 
                                new(Int32, Int32)[] {(low, high - low + 1) });
                            NotifyOnIterationCompleted(args);
                        }
                    }

                    if (i <= j)
                    {
                        i++;
                        j--;
                    }
                }

                Int32 p = i;

                this.ObservableQuickSort(low, p - 1, notify, ref it);
                this.ObservableQuickSort(p, high, notify, ref it);
            }
        }

        public void QuickSort()
        {
            Int32 it = 0;
            this.ObservableQuickSort(0, this.Count - 1, false, ref it);
        }
        public void ObservableQuickSort()
        {
            Int32 it = 0;
            if (this.Count > 50) throw new ArgumentException("Слишком много данных для отображения");
            this.ObservableQuickSort(0, this.Count - 1, true, ref it);
        }

        public ISortableCollection Clone()
        {
            Int32 n = this.Count;
            var sc = new SortableCollection();

            foreach (var item in this)
            {
                sc.Add(item);
            }
            return sc;
        }
    }    
}
