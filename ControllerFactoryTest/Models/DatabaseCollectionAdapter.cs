﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerFactoryTest.Models
{
    public class DataBaseCollectionAdapter : ISortableCollection
    {
        private static MergeSortContext m_db = MergeSortContext.GetContext();

        public int this[int index]
        {
            get => m_db.ANodes.OrderBy(t => t.Id).Skip(index).First();
            set
            {
                ANode a = m_db.ANodes.OrderBy(t => t.Id).Skip(index).First();
                a.Value = value;

                m_db.SaveChanges();
            }
        }
        public DataBaseCollectionAdapter()
        {
            m_db.ANodes.RemoveRange(m_db.ANodes);
            m_db.BNodes.RemoveRange(m_db.BNodes);
            m_db.CNodes.RemoveRange(m_db.CNodes);
        }

        public int Count { get; private set; } = 0;
        public bool IsReadOnly => false;
        public event EventHandler<IterationCompletedEventArgs> IterationCompleted = delegate { };
        public void Add(int item)
        {
            m_db.ANodes.Add(item);
            m_db.SaveChanges();
            Count++;
        }
        public void Clear()
        {
            m_db.ANodes.RemoveRange(m_db.ANodes);
            m_db.SaveChanges();
            Count = 0;
        }

        public bool Contains(int item)
        {
            return m_db.ANodes.AsNoTracking().Any(t => t.Value == item);
        }
        public void CopyTo(int[] array, int arrayIndex)
        {
            for (Int32 i = 0; i < Count; i++)
            {
                array[arrayIndex + i] = this[i];
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return new DataBaseCollectionAdapterEnumerator(this);
        }
        public int IndexOf(int item)
        {
            for (Int32 i = 0; i < Count; i++)
            {
                if (this[i] == item) return i;
            }
            return -1;
        }
        public void Insert(int index, int item)
        {
            this[index] = item;
        }
        public bool Remove(int item)
        {
            Count--;
            return m_db.ANodes.Remove(item) != null;
        }
        public void RemoveAt(int index)
        {
            m_db.ANodes.Remove(this[index]);
            m_db.SaveChanges();
            Count--;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void NotifyOnIterationCompleted(IterationCompletedEventArgs e)
        {
            IterationCompleted(this, e);
        }
        public ISortableCollection Clone()
        {
            return this;
        }

        public void MergeSort()
        {
            ObservableMergeSort(false);
        }
        public void ObservableMergeSort()
        {
            if (this.Count > 100) throw new ArgumentException("Слишком много данных для отображения");
            this.ObservableMergeSort(true);
        }
        public void ObservableMergeSort(Boolean notify)
        {
            Int32 k = 1, n = this.Count;
            Int32 it = 1;

            String folder = Guid.NewGuid().ToString();
            var dir = Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/" + folder);
            while (!dir.Exists) ;

            if (notify)
            {

                
                IterationCompletedEventArgs e = new IterationCompletedEventArgs(this, it++, -1, -1, null,
                    Enumerable.Range(0, (n + 1) / 2).Select(x => (2 * x, Math.Min(1, n - 2 * x))).ToList());
                IterationCompleted(this, e);
            }

            while (k < n)
            {
                StreamWriter swB = new StreamWriter(folder + "/" + (it - 1) + "_B.txt", false, System.Text.Encoding.Default);
                StreamWriter swC = new StreamWriter(folder + "/" + (it - 1) + "_C.txt", false, System.Text.Encoding.Default);
                Int32 i = 0;

                for (; i < n - 2 * k; i += 2 * k)
                {
                    for (Int32 j = 0; j < k; j++)
                    {
                        m_db.BNodes.Add(this[i + j]);
                        m_db.CNodes.Add(this[i + k + j]);
                        m_db.SaveChanges();
                    }
                }
                Int32 jStop1 = Math.Min(k, n - i);

                for (Int32 j = 0; j < jStop1; j++)
                {
                    m_db.BNodes.Add(this[i + j]);
                    m_db.SaveChanges();
                }
                i += jStop1;

                for (; i < n; i++)
                {
                    m_db.CNodes.Add(this[i]);
                    m_db.SaveChanges();
                }

                Int32 bsLeft, csLeft;
                for (i = 0; i < n; i += 2 * k)
                {
                    bsLeft = Math.Min(k, n - i);
                    csLeft = Math.Min(k, n - i - bsLeft);
                    Int32 jStop = bsLeft + csLeft;

                    for (Int32 j = 0; j < jStop; j++)
                    {
                        BNode b = m_db.BNodes.FirstOrDefault(x => bsLeft > 0);
                        CNode c = m_db.CNodes.FirstOrDefault(x => csLeft > 0);
                        Node res;

                        if (b == null || c?.Value < b.Value)
                        {
                            res = c;
                            csLeft--;
                            swC.Write(res.Value + " ");
                        }
                        else
                        {
                            res = b;
                            bsLeft--;
                            swB.Write(res.Value + " ");
                        }

                        this[i + j] = res.Value;

                        m_db.Entry(res).State = EntityState.Deleted;
                        m_db.SaveChanges();
                    }
                }

                k *= 2;

                if (notify)
                {
                    IterationCompletedEventArgs e = new IterationCompletedEventArgs(this, it++, -1, -1, null,
                        Enumerable.Range(0, (n + 2 * k - 1) / (2 * k)).Select(x => (2 * x * k, Math.Min(k, n - 2 * x * k))).ToList());
                    IterationCompleted(this, e);
                }
                swB.Dispose();
                swC.Dispose();
            }
        }

        private class DataBaseCollectionAdapterEnumerator : IEnumerator<Int32>
        {
            private DataBaseCollectionAdapter m_col;
            private Int32 m_index = -1;
            public int Current
            {
                get
                {
                    if (m_index < 0 || m_index >= m_col.Count)
                        throw new IndexOutOfRangeException();
                    return m_col[m_index];
                }
            }

            object IEnumerator.Current => Current;
            public DataBaseCollectionAdapterEnumerator(DataBaseCollectionAdapter col)
            {
                m_col = col;
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return ++m_index < m_col.Count;
            }

            public void Reset()
            {
                m_index = 0;
            }
        }
    }
}
