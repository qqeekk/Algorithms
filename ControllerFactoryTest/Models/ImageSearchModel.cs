﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerFactoryTest.Models
{
    public class ImageSearchModel
    {
        public int FindImage(bool isCaseSensitive, string text, string image)
        {
            int win1251_capacity = 256;
            Encoding enc = Encoding.GetEncoding(1251);

            if (!isCaseSensitive)
            {
                text = text.ToLower();
                image = image.ToLower();
            }

            byte[] imageBytes = enc.GetBytes(image);
            byte[] textBytes = enc.GetBytes(text);

            //заполнение массива отступов для символов кодировки 
            var mass = Enumerable.Repeat(imageBytes.Length, win1251_capacity).ToArray();

            for (int i = imageBytes.Length - 1; i >= 0; i--)
            {
                mass[imageBytes[i]] = Math.Min(imageBytes.Length - i - 1, mass[imageBytes[i]]);
            }

            int imageEndPos = imageBytes.Length - 1;
            bool broken = false;

            while (imageEndPos < textBytes.Length)
            {
                broken = false;

                int i;
                int imageStartPos = imageEndPos - imageBytes.Length + 1;

                for (i = imageEndPos; i >= imageStartPos; i--)
                {
                    if (textBytes[i] != imageBytes[i - imageStartPos])
                    {
                        imageEndPos += mass[textBytes[i]] + i - imageEndPos;
                        broken = true;
                        break;
                    }
                }

                if (!broken)
                {
                    return i + 1;
                }
            }
            return -1;
        }
    }
}
