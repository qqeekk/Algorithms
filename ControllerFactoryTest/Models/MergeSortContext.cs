﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerFactoryTest.Models
{
    public abstract class Node
    {
        public Int32 Id { get; set; }
        [Required]
        public Int32 Value { get; set; }

        public static implicit operator Int32(Node x)
        {
            return x.Value;
        }
    }
    public class ANode : Node
    {
        public static implicit operator ANode(Int32 x)
        {
            return new ANode { Value = x };
        }
    }
    public class CNode : Node
    {
        public static implicit operator CNode(Int32 x)
        {
            return new CNode { Value = x };
        }
    }
    public class BNode : Node
    {
        public static implicit operator BNode(Int32 x)
        {
            return new BNode { Value = x };
        }
    }
    public class MergeSortContext : DbContext
    {
        private static MergeSortContext m_instance;
        public static MergeSortContext GetContext()
        {
            if(m_instance == null)
            {
                m_instance = new MergeSortContext();
            }
            return m_instance;
        }

        internal MergeSortContext() : base("DbConnection") { }
        public DbSet<ANode> ANodes { get; set; }
        public DbSet<BNode> BNodes { get; set; }
        public DbSet<CNode> CNodes { get; set; }
    }

}
