﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ControllerFactoryTest
{
    using ControllerFactoryTest.Models;
    using System.Windows.Navigation;

    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //получение коллекции типов(классов), определенных в данной сборке
            var allTypes = Assembly.GetExecutingAssembly().GetTypes();

            var providerTypes = allTypes.Where(
                t => t.IsClass && 
                t.GetInterfaces().Contains(typeof(IViewProvider))
            );

            var providers = from Type t in providerTypes
                            let inst = Activator.CreateInstance(t) as IViewProvider
                            orderby inst.Priority
                            select inst;

            Resources["WindowCreators"] = providers;
            var db = MergeSortContext.GetContext();
            db.BNodes.RemoveRange(db.BNodes);
            db.CNodes.RemoveRange(db.CNodes);

            MainWindow window = new MainWindow() { Height = 700, Width = 600 };
            window.Show();
            window.Closing += (sender1, e1) => db.Dispose();
        }
    }
}
