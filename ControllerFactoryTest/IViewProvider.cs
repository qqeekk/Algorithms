﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ControllerFactoryTest
{
    interface IViewProvider
    {
        string DisplayName { get; }
        int Priority { get; }
        Page CreateView(); 
    }
}
