﻿using ControllerFactoryTest.Models;
using ControllerFactoryTest.ViewModels;
using ControllerFactoryTest.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ControllerFactoryTest.ViewProviders
{
    class ImageSearchViewProvider : IViewProvider
    {
        public string DisplayName => "Лабораторная 1. Поиск образа в строке";
        public int Priority => 1;

        public Page CreateView()
        {
            ImageSearchModel model = new ImageSearchModel();
            ImageSearchViewModel vm = new ImageSearchViewModel(model);
            return new ImageSearchView(DisplayName, vm);
        }
    }
}
