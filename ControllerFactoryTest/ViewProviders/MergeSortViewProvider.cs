﻿using ControllerFactoryTest.Models;
using ControllerFactoryTest.ViewModels;
using ControllerFactoryTest.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ControllerFactoryTest.ViewProviders
{
    class MergeSortViewProvider : IViewProvider
    {
        public string DisplayName => "Лабораторная 4. Внешние сортировки";

        public int Priority => 4;

        public Page CreateView()
        {
            var mergeSort = new CollectionActivityProvider("Сортировка слиянием", 
                list => ((DataBaseCollectionAdapter)list).MergeSort(), 
                list => ((DataBaseCollectionAdapter)list).ObservableMergeSort());
            var activities = new CollectionActivityProvider[] { mergeSort };

            Page page = new LameSort(new LameSortViewModel<DataBaseCollectionAdapter>(activities)) { Title = DisplayName};
            
            return page;
        }
    }
}
