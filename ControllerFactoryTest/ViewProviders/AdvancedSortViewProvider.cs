﻿using ControllerFactoryTest.Models;
using ControllerFactoryTest.ViewModels;
using ControllerFactoryTest.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ControllerFactoryTest.ViewProviders
{
    class AdvancedSortViewProvider : IViewProvider
    {
        public string DisplayName => "Лабораторная 3. Быстрые сортировки";

        public int Priority => 3;

        public Page CreateView()
        {
            var activity = new CollectionActivityProvider("Сортировка Шелла", 
                list => ((SortableCollection)list).ShellSort(), 
                list => ((SortableCollection)list).ObservableShellSort());
            var activity2 = new CollectionActivityProvider("Быстрая сортировка",
                list => ((SortableCollection)list).QuickSort(),
                list => ((SortableCollection)list).ObservableQuickSort());
            var vm = new LameSortViewModel<SortableCollection>(new CollectionActivityProvider[] { activity, activity2 });
            return new LameSort(vm) { Title = DisplayName };
        }
    }
}
