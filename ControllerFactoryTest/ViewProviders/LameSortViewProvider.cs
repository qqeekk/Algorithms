﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ControllerFactoryTest.ViewProviders
{
    using Views;
    using ViewModels;
    using ControllerFactoryTest.Models;

    public class CollectionActivityProvider
    {
        public Int32 MaxElement { set; private get; }
        public Int32 MinElement { set; private get; }
        public String ActivityName { get; }

        public Action<ISortableCollection> Invoke { get; set; }
        public Action<ISortableCollection> InvokeObservable { get; set; }

        public CollectionActivityProvider(
            String activityName, 
            Action<ISortableCollection> activity,
            Action<ISortableCollection> obsActivity)
        {
            ActivityName = activityName;
            Invoke = activity;
            InvokeObservable = obsActivity;
        }
    }

    class LameSortViewProvider : IViewProvider
    {
        public string DisplayName => "Лабораторная 2. Квадратичные сортировки";

        public int Priority => 2;

        public Page CreateView()
        {
            var ChooseSort = new CollectionActivityProvider("Сортировка выбором",
                list => ((SortableCollection)list).SelectionSort(),
                list => ((SortableCollection)list).ObservableSelectionSort());
            var BubbleSort = new CollectionActivityProvider("Сортировка пузырьком",
                list => ((SortableCollection)list).BubbleSort(), 
                list => ((SortableCollection)list).ObservableBubbleSort());

            var activities = new CollectionActivityProvider[] { ChooseSort, BubbleSort };
            var vm = new LameSortViewModel<SortableCollection>(activities);
            return new LameSort(vm) { Title = DisplayName };
        }
    }
}
