﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using ControllerFactoryTest.Models;
using ControllerFactoryTest.ViewProviders;

namespace ControllerFactoryTest.ViewModels
{
    public interface ILameSortViewModel : INotifyPropertyChanged
    {
        Int32 GeneratedAmount { get; set; }
        String OutputString { get; }
        String InputString { get; set; }
        Int32 MaxElement { get; set; }
        Int32 MinElement { get; set; }
        TimeSpan TimeElapsed { get; set; }
        ISortableCollection Massive { get; set; }
        StringBuilder OutputStringBuilder { set; }

        ICommand GenerateElementsCommand { get; }
        ICommand RunObservableSortingCommand { get; }
        ICommand RunSortingCommand { get; }
        ICommand TestSortingCommand { get; }

        CollectionActivityProvider SelectedOption { get; set; }
        CollectionActivityProvider[] SelectOptions { get; }

        IList<(int, int)> SwappedElementsEffects { get; }
        IList<(int, int)> TaggedElementsEffects { get; }
        IList<(int, int)> RangedElementsEffects { get; }
        event EventHandler<EventArgs> TextEffectsChanged;

        void UpdateOutput();
        void OnObservableSortingIterationCompleted(object sender, IterationCompletedEventArgs e);
    }
}