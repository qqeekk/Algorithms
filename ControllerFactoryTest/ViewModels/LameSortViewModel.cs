﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ControllerFactoryTest.ViewModels
{
    using ControllerFactoryTest.ViewProviders;
    using Models;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Data;

    public abstract class LameSortCommandBase
    {
        protected ILameSortViewModel ViewModel;
        protected LameSortCommandBase(ILameSortViewModel vm)
        {
            ViewModel = vm;
            vm.PropertyChanged += ViewModelPropertyChanged;
        }

        private void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }

    public class RunSortingCommand : LameSortCommandBase, ICommand
    {
        public RunSortingCommand(ILameSortViewModel vm) : base(vm) { }

        public Boolean CanExecute(object parameter)
        {
            return ViewModel.SelectedOption != null 
                && ViewModel.Massive != null
                && ViewModel.Massive.Count != 0;
        }
        public void Execute(object parameter)
        {
            if(parameter is CollectionActivityProvider ap)
            {
                Stopwatch sw = new Stopwatch();
                try
                {
                    sw.Start();
                    ap.Invoke(ViewModel.Massive);
                    sw.Stop();

                    ViewModel.TimeElapsed = sw.Elapsed;
                    ViewModel.OutputStringBuilder = new StringBuilder(ViewModel.Massive.ToContentString());
                    ViewModel.UpdateOutput();

                }
                catch (Exception ex)
                {
                    sw.Stop();
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
    public class GenerateElementsCommand<T> : LameSortCommandBase, ICommand
        where T : ISortableCollection, new()
    {
        public GenerateElementsCommand(ILameSortViewModel vm) : base(vm) { }

        public Boolean CanExecute(Object parameter)
        {
            return ViewModel.MinElement <= ViewModel.MaxElement 
                && ViewModel.GeneratedAmount > 0 
                && ViewModel.GeneratedAmount < 2000;
        }
        public void Execute(Object parameter)
        {
            Random random = new Random();
            Int32 min = ViewModel.MinElement;
            Int32 max = ViewModel.MaxElement;
            Int32 count = ViewModel.GeneratedAmount;

            var list = new T();
            for (Int32 i = 0; i < count; i++)
            {
                list.Add(random.Next(min, max + 1));
            }

            ViewModel.Massive = list;
        }
    }
    public class TestSortingCommand : LameSortCommandBase, ICommand
    {
        public TestSortingCommand(ILameSortViewModel vm) : base(vm) { }

        public Boolean CanExecute(object parameter)
        {
            return ViewModel.Massive != null
                && ViewModel.Massive.Count != 0
                && ViewModel.SelectOptions != null
                && ViewModel.SelectOptions.Length != 0;
        }
        public void Execute(object parameter)
        {
            StringBuilder str = new StringBuilder();
            var list = ViewModel.Massive;

            foreach(var activity in ViewModel.SelectOptions)
            {
                Stopwatch sw = new Stopwatch();
                str.Append($"{activity.ActivityName}: ");
                var cpylist = list.Clone();

                try
                {

                    sw.Start();
                    activity.Invoke(cpylist);
                    sw.Stop();
                    str.Append(sw.Elapsed);
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    str.Append(ex.Message);
                }

                str.AppendLine();
            }

            MessageBox.Show(str.ToString());
        }
    }
    public class RunObservableSortingCommand : LameSortCommandBase, ICommand
    {
        public RunObservableSortingCommand(ILameSortViewModel vm) : base(vm) { }

        public Boolean CanExecute(object parameter)
        {
            return ViewModel.Massive != null
                && ViewModel.Massive.Count != 0
                && ViewModel.SelectedOption != null;
        }

        public void Execute(Object parameter)
        {
            ISortableCollection list = ViewModel.Massive;
            ViewModel.OutputStringBuilder = new StringBuilder();

            list.IterationCompleted += ViewModel.OnObservableSortingIterationCompleted;

            if(parameter is CollectionActivityProvider activity)
            {
                try
                {
                    activity.InvokeObservable(list);
                    ViewModel.UpdateOutput();
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }
    }

    public sealed class LameSortViewModel<T> : INotifyPropertyChanged, ILameSortViewModel 
        where T : ISortableCollection, new()
    {
        private TimeSpan m_timeElapsed;
        private ISortableCollection m_massive = new T();
        private String m_currentMassiveString;
        private Int32 m_generatedAmount;
        private Int32 m_maxElement;
        private Int32 m_minElement;
        private CollectionActivityProvider m_selectedOption;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public void NotifyPropertyChanged(String name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public ISortableCollection Massive
        {
            get => m_massive;
            set
            {
                m_massive = value;
                m_currentMassiveString = m_massive?.ToContentString();
                
                NotifyPropertyChanged(nameof(Massive));
                NotifyPropertyChanged(nameof(InputString));
            }
        }
        public StringBuilder OutputStringBuilder { private get; set; }

        public String InputString
        {
            get
            {
                return m_currentMassiveString;
            }
            set
            {
                m_currentMassiveString = value;
                m_massive = ICollectionParsingExtensions.FromContentString<T>(value);
                NotifyPropertyChanged(nameof(Massive));
            }
        }
        public String OutputString
        {
            get => OutputStringBuilder?.ToString();
        }
        public Int32 GeneratedAmount
        {
            get { return m_generatedAmount; }
            set
            {
                m_generatedAmount = value;
                NotifyPropertyChanged(nameof(GeneratedAmount));
            }
        }
        public Int32 MinElement
        {
            get { return m_minElement; }
            set
            {
                m_minElement = value;
                NotifyPropertyChanged(nameof(MinElement));
            }
        }
        public Int32 MaxElement
        {
            get { return m_maxElement; }
            set
            {
                m_maxElement = value;
                NotifyPropertyChanged(nameof(MaxElement));
            }
        }
        public CollectionActivityProvider[] SelectOptions { get; }
        public CollectionActivityProvider SelectedOption
        {
            get => m_selectedOption;
            set
            {
                m_selectedOption = value;
                NotifyPropertyChanged(nameof(SelectedOption));
            }
        }

        public ICommand GenerateElementsCommand { get; }
        public ICommand RunSortingCommand { get; }
        public ICommand TestSortingCommand { get; }
        public ICommand RunObservableSortingCommand { get; }
        public TimeSpan TimeElapsed
        {
            get => m_timeElapsed;
            set
            {
                m_timeElapsed = value;
                NotifyPropertyChanged(nameof(TimeElapsed));
            }
        }
        public IList<(Int32, Int32)> SwappedElementsEffects { get; } = new List<(Int32, Int32)>();
        public IList<(Int32, Int32)> TaggedElementsEffects { get; } = new List<(Int32, Int32)>();
        public IList<(Int32, Int32)> RangedElementsEffects { get; } = new List<(Int32, Int32)>();

        public LameSortViewModel(CollectionActivityProvider[] selectOptions)
        {
            SelectOptions = selectOptions ?? Array.Empty<CollectionActivityProvider>();
            SelectedOption = SelectOptions[0];
            GenerateElementsCommand = new GenerateElementsCommand<T>(this);
            RunSortingCommand = new RunSortingCommand(this);
            TestSortingCommand = new TestSortingCommand(this);
            RunObservableSortingCommand = new RunObservableSortingCommand(this);
        }
        public event EventHandler<EventArgs> TextEffectsChanged = delegate { };

        public void UpdateOutput()
        {
            NotifyPropertyChanged(nameof(OutputString));
            TextEffectsChanged(this, EventArgs.Empty);

            SwappedElementsEffects.Clear();
            TaggedElementsEffects.Clear();
            RangedElementsEffects.Clear();
        }
        public void OnObservableSortingIterationCompleted(object sender, IterationCompletedEventArgs e)
        {
            Int32 to = e.SwappedToPosition, from = e.SwappedFromPosition;
            var tagged = e.TaggedPositions;
            var ranged = e.RangedPositions;
            String str = e.ChangedList.ToContentString(ref from, out Int32 fromCount, ref to, out Int32 toCount, 
                tagged, out IList<(Int32, Int32)> tags, ranged, out IList<(Int32 pos, Int32 len)> ranges);

            OutputStringBuilder.AppendFormat($"{e.Iteration,2}. ");
            SwappedElementsEffects.Add((OutputStringBuilder.Length + from, fromCount));
            SwappedElementsEffects.Add((OutputStringBuilder.Length + to, toCount));

            foreach (var (pos, len) in tags)
                TaggedElementsEffects.Add((OutputStringBuilder.Length + pos, len));
            foreach (var (pos, len) in ranges)
                RangedElementsEffects.Add((OutputStringBuilder.Length + pos, len));
            
            OutputStringBuilder.AppendFormat(str + Environment.NewLine);
        }
    }
}
