﻿using ControllerFactoryTest.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ControllerFactoryTest.ViewModels
{
    public class ImageFoundEventArgs : EventArgs
    {
        public bool IsFound { get; set; }
        public string ObjectText { get; set; }
        public int StartPosition { get; set; }
        public int ImageLength { get; set; }
    }

    internal class ImageSearchButtonCommand : ICommand
    {
        private ImageSearchViewModel _vm;
        private ImageSearchModel _model;

        public event EventHandler CanExecuteChanged = delegate { };
        public ImageSearchButtonCommand(ImageSearchViewModel vm, ImageSearchModel model)
        {
            vm.PropertyChanged += vm_PropertyChanged;
            _vm = vm;
            _model = model;
        }

        private void vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        public bool CanExecute(object parameter)
        {
            return _vm.Image != null && _vm.ObjectText != null;
        }
        public void Execute(object parameter)
        {
            int index = _model.FindImage(_vm.IsCaseSensitive, text: _vm.ObjectText, image: _vm.Image);
            _vm.NotifyImageFound(index != -1, _vm.ObjectText, index, _vm.Image.Length);
        }
    }

    public class ImageSearchViewModel : INotifyPropertyChanged
    {
        private static readonly Encoding encoding = Encoding.GetEncoding(1251);
        public delegate void ImageFoundEventHandler(object sender, ImageFoundEventArgs e);

        public ICommand ButtonCommand { get; }

        private byte[] _textBytes;
        private byte[] _imageBytes;

        private ImageSearchModel _model;
        public string ObjectText
        {
            get
            {
                if (_textBytes == null) return null;
                return encoding.GetString(_textBytes);
            }
            set
            {
                _textBytes = encoding.GetBytes(value);
                NotifyPropertyChanged(nameof(ObjectText));
            }
        }
        public string Image
        {
            get
            {
                if (_imageBytes == null) return null;
                return encoding.GetString(_imageBytes);
            }
            set
            {
                _imageBytes = encoding.GetBytes(value);
                NotifyPropertyChanged(nameof(Image));
            }
        }

        public bool IsCaseSensitive { get; set; } = true;

        public ImageSearchViewModel(ImageSearchModel model)
        {
            _model = model;
            ButtonCommand = new ImageSearchButtonCommand(this, model);
        }

        public event ImageFoundEventHandler ImageFound;
        public event PropertyChangedEventHandler PropertyChanged;
        
        public void NotifyPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        public void NotifyImageFound(bool isFound, string text, int pos, int imageLength)
        {
            var args = new ImageFoundEventArgs()
            {
                ObjectText = text,
                ImageLength = imageLength,
                StartPosition = pos,
                IsFound = isFound
            };
            ImageFound(this, args);
        }
    }
}
