﻿using ControllerFactoryTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControllerFactoryTest.Views
{
    /// <summary>
    /// Логика взаимодействия для ImageSearch.xaml
    /// </summary>
    public partial class ImageSearchView : Page
    {
        public ImageSearchView(string title, ImageSearchViewModel vm)
        {
            InitializeComponent();

            vm.ImageFound += OnImageFound;
            DataContext = vm;
            Title = title;
        }

        private void OnImageFound(object sender, ImageFoundEventArgs e)
        {
            if (!e.IsFound || e == ImageFoundEventArgs.Empty)
            {
                VisualisationPanel.Visibility = Visibility.Collapsed;
                MessageBox.Show("Не найдено!");
                return;
            }

            ImageFoundTextBlock.Text = e.ObjectText;

            var texteff = ImageFoundTextBlock.TextEffects[0];
            texteff.PositionStart = e.StartPosition;
            texteff.PositionCount = e.ImageLength;
            texteff.Foreground = new SolidColorBrush(Colors.Red);

            VisualisationPanel.Visibility = Visibility.Visible;
        }
    }
}
