﻿using ControllerFactoryTest.ViewModels;
using ControllerFactoryTest.ViewProviders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControllerFactoryTest.Views
{
    /// <summary>
    /// Логика взаимодействия для LameSort.xaml
    /// </summary>
    public partial class LameSort : Page
    {
        ILameSortViewModel m_vm;
        public LameSort(ILameSortViewModel vm)
        {
            InitializeComponent();
            DataContext = m_vm = vm;
            vm.TextEffectsChanged += Vm_TextEffectsChanged;
            foreach (var option in vm.SelectOptions)
            {
                var rb = new RadioButton()
                {
                    GroupName = "CommonSelection",
                    Tag = option,
                    Content = option.ActivityName,
                    IsChecked = option == vm.SelectedOption
                };
                rb.Checked += RadioButton_Checked;
                RadioButtons.Items.Add(rb);
            }
        }

        private void Vm_TextEffectsChanged(object sender, EventArgs e)
        {
            tbxOutput.TextEffects.Clear();
            
            foreach (var (pos, len) in m_vm.RangedElementsEffects)
            {
                var effect = new TextEffect
                {
                    PositionStart = pos,
                    PositionCount = len,
                    Foreground = Brushes.Blue
                };

                tbxOutput.TextEffects.Add(effect);
            }

            foreach (var (pos, len) in m_vm.TaggedElementsEffects)
            {
                var effect = new TextEffect
                {
                    PositionStart = pos,
                    PositionCount = len,
                    Foreground = Brushes.Green
                };

                tbxOutput.TextEffects.Add(effect);
            }

            foreach (var (pos, len) in m_vm.SwappedElementsEffects)
            {
                var effect = new TextEffect
                {
                    PositionStart = pos,
                    PositionCount = len,
                    Foreground = Brushes.Red
                };

                tbxOutput.TextEffects.Add(effect);
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if(e.Source is RadioButton rb)
            {
                if (rb.IsChecked == true)
                    m_vm.SelectedOption = rb.Tag as CollectionActivityProvider;
            }
        }
    }
}
