# Algorithms
### About
This WPF-application represents how different searching and sorting algorithms work.

![Starting page](https://pp.userapi.com/c834301/v834301235/116a62/EbOos7HaXIU.jpg)

### Boyer–Moore string search algorithm
The key features of the algorithm are to match on the tail of the pattern rather than the head, and to skip along the text in jumps of multiple characters rather than searching every single character in the text. The implementation of fast substring searching algorithms can be found [here](ControllerFactoryTest/Models/ImageSearchModel.cs).

![Boyer-Moore](https://pp.userapi.com/c847217/v847217235/2da9d/IyWHUu4UVao.jpg)

### Bubble sort algorithm
One of the simplest sorting algorithm with the complexity O(n^2). The implementation of this algorithm can be found [here](https://gitlab.com/qqeekk/Algorithms/blob/master/ControllerFactoryTest/Models/SortableCollection.cs).

![Bubble Sort](https://pp.userapi.com/c846020/v846020786/2dd61/oUIlQLAQZSE.jpg)

### Selection sort algorithm
Array's divided in two parts. The first part is sorted and it gets larger every iteration with the elements from the second, unsorted part. The implementation of this algorithm can be found [here](https://gitlab.com/qqeekk/Algorithms/blob/master/ControllerFactoryTest/Models/SortableCollection.cs).

![Selection Sort](https://pp.userapi.com/c844724/v844724786/33a18/XonlpII75sA.jpg)

### Shell's sort algorithm
Shell's method, is an in-place comparison sort. The method starts by sorting pairs of elements far apart from each other, then progressively reducing the gap between elements to be compared. Starting with far apart elements, it can move some out-of-place elements into position faster than a simple nearest neighbor exchange. Link [here](https://gitlab.com/qqeekk/Algorithms/blob/master/ControllerFactoryTest/Models/SortableCollection.cs).

![Shell Sort](https://pp.userapi.com/c845323/v845323786/33775/iuzolMzm5c8.jpg)

### Quicksort algorithm
Quicksort is a divide and conquer algorithm. Quicksort first divides a large array into two smaller sub-arrays: the low elements and the high elements. Quicksort can then recursively sort the sub-arrays. It's one of the most efficient algorithms, that takes, on average, O(n*log(n)) comparisons to sort n items. Link [here](https://gitlab.com/qqeekk/Algorithms/blob/master/ControllerFactoryTest/Models/SortableCollection.cs).

![Quicksort](https://pp.userapi.com/c846021/v846021951/2df41/ZlQhfTwqUW8.jpg)

### Merge sort algorithm
An external sorting algorithm with the use of databases. An implementation of the algorithm can be found [here](https://gitlab.com/qqeekk/Algorithms/blob/master/ControllerFactoryTest/Models/DatabaseCollectionAdapter.cs).

![Merge Sort](https://pp.userapi.com/c846021/v846021951/2df7e/Rw7-h8RaQrM.jpg)